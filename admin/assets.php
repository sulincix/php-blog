<!DOCTYPE html>
<?php 
$target="assets.php";
include "user.php";
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Assets Manager</title>
</head>
<body>
    <form action="?" method="post" enctype="multipart/form-data">
        Upload a Assets:
        <input type="file" name="the_file" id="fileToUpload">
        <input type="submit" name="submit" value="Start Upload">
    </form>
</body>
</html>
<?php
    $currentDirectory = getcwd();
    if(is_dir("/../assets/")==False){
			mkdir("/../assets/");
	}
    $uploadDirectory = "/../assets/";

    $errors = []; // Store errors here


    $fileName = $_FILES['the_file']['name'];
    $fileSize = $_FILES['the_file']['size'];
    $fileTmpName  = $_FILES['the_file']['tmp_name'];
    $fileType = $_FILES['the_file']['type'];

    $uploadPath = $currentDirectory . $uploadDirectory . basename($fileName); 

    if (isset($_POST['submit'])) {
        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
      }

?>
<?php 
$srcdir="../assets";
if ($handle = opendir($srcdir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            print("<p>".$entry."\n");
			print("<a target=\"al\" href=\"/assets/".$entry."\">view</a>\n");
			print("<a href=\"delassets.php?file=".$entry."\">delete</a></p>\n");
        }
    }
    closedir($handle);
	print("<a href=\"main.php\"><b>Go to Main Page</b></a>\n");
}
?>

