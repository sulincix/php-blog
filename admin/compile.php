<?php
$target="compile.php";
include "user.php";
$srcdir="../src";
$custdir="../custom";
function compile($location){
	$target=str_replace(".html",".php",$location);
	$f=fopen('../'.$target,"w");
	$content=fopen('../src/'.$location,"r");
	$begin=fopen('../template/begin','r');
	$end=fopen('../template/end','r');
	fwrite($f,fread($begin,filesize('../template/begin')));
	fwrite($f,fread($content,filesize('../src/'.$location))."\n");
	fwrite($f,fread($end,filesize('../template/end')));
}
function compile_cust($location){
	$target=$location.".php";
	$f=fopen('../'.$target,"w");
	$content=fopen('../custom/'.$location,"r");
	fwrite($f,fread($content,filesize('../custom/'.$location))."\n");
}

print("<br><b>Clearing Workdir</b></br>");
if ($handle = opendir("..")) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
			$entry=str_replace("..",".",$entry);
            if(is_file("../".$entry)){
				unlink("../".$entry);
			}
        }
    }
    closedir($handle);
}
if ($handle = opendir($custdir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
			$entry=str_replace("..",".",$entry);
            print("<br>Compiling:<b>".$entry."</b>(Custom Page)</br>");
            compile_cust($entry);
        }
    }
    closedir($handle);
}
if ($handle = opendir($srcdir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
			$entry=str_replace("..",".",$entry);
            print("<br>Compiling:<b>".$entry."</b></br>");
            compile($entry);
        }
    }
    print('<br>Done:<b><a href="index.html">Go to Main Page</a></b></br>');
    closedir($handle);
}
?>
